���6      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`67b2e32`�h]�h �	reference���)��}�(h�git commit 67b2e32�h]�h �Text����git commit 67b2e32�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/67b2e32�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7fd1528816a8>`

�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7fd1528816a8>�h]�h�<git commit <function get_git_version_full at 0x7fd1528816a8>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7fd1528816a8>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _trackpoints:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��trackpoints�uh0h]h:Kh hhhh8�7/home/whot/code/libinput/build/doc/user/trackpoints.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Trackpoints and Pointing Sticks�h]�h�Trackpoints and Pointing Sticks�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(h��This page provides an overview of trackpoint handling in libinput, also
refered to as Pointing Stick or Trackstick. The device itself is usually a
round plastic stick between the G, H and B keys with a set of buttons below
the space bar.�h]�h��This page provides an overview of trackpoint handling in libinput, also
refered to as Pointing Stick or Trackstick. The device itself is usually a
round plastic stick between the G, H and B keys with a set of buttons below
the space bar.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh �figure���)��}�(hhh]�(h �image���)��}�(h�F.. figure:: button-scrolling.svg
    :align: center

    A trackpoint
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��button-scrolling.svg��
candidates�}��*�h�suh0h�h h�h8hkh:Kubh �caption���)��}�(h�A trackpoint�h]�h�A trackpoint�����}�(hh�h h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�ubeh!}�(h#]��id1�ah%]�h']�h)]�h+]��align��center�uh0h�h:Kh hnhhh8hkubh�)��}�(h��libinput always treats the buttons below the space bar as the buttons that
belong to the trackpoint even on the few laptops where the buttons are not
physically wired to the trackpoint device anyway, see :ref:`t440_support`.�h]�(h��libinput always treats the buttons below the space bar as the buttons that
belong to the trackpoint even on the few laptops where the buttons are not
physically wired to the trackpoint device anyway, see �����}�(h��libinput always treats the buttons below the space bar as the buttons that
belong to the trackpoint even on the few laptops where the buttons are not
physically wired to the trackpoint device anyway, see �h h�hhh8Nh:Nub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`t440_support`�h]�h �inline���)��}�(hh�h]�h�t440_support�����}�(hhh h�ubah!}�(h#]�h%]�(�xref��std��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�h݌refexplicit���	reftarget��t440_support��refdoc��trackpoints��refwarn��uh0h�h8hkh:Kh h�ubh�.�����}�(h�.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh^)��}�(h�.. _trackpoint_buttonscroll:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�trackpoint-buttonscroll�uh0h]h:Kh hnhhh8hkubhm)��}�(hhh]�(hr)��}�(h�Button scrolling on trackpoints�h]�h�Button scrolling on trackpoints�����}�(hj  h j
  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j  hhh8hkh:Kubh�)��}�(h��Trackpoint devices have :ref:`button_scrolling` enabled by default. This may
interfer with middle-button dragging, if middle-button dragging is required
by a user then button scrolling must be disabled.�h]�(h�Trackpoint devices have �����}�(h�Trackpoint devices have �h j  hhh8Nh:Nubh�)��}�(h�:ref:`button_scrolling`�h]�h�)��}�(hj#  h]�h�button_scrolling�����}�(hhh j%  ubah!}�(h#]�h%]�(h܌std��std-ref�eh']�h)]�h+]�uh0h�h j!  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j/  �refexplicit��h�button_scrolling�h�h�h��uh0h�h8hkh:Kh j  ubh�� enabled by default. This may
interfer with middle-button dragging, if middle-button dragging is required
by a user then button scrolling must be disabled.�����}�(h�� enabled by default. This may
interfer with middle-button dragging, if middle-button dragging is required
by a user then button scrolling must be disabled.�h j  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j  hhubh^)��}�(h�.. _trackpoint_range:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�trackpoint-range�uh0h]h:K$h j  hhh8hkubeh!}�(h#]�(�button-scrolling-on-trackpoints�j  eh%]�h']�(�button scrolling on trackpoints��trackpoint_buttonscroll�eh)]�h+]�uh0hlh hnhhh8hkh:K�expect_referenced_by_name�}�j[  h�s�expect_referenced_by_id�}�j  h�subhm)��}�(hhh]�(hr)��}�(h�Motion range on trackpoints�h]�h�Motion range on trackpoints�����}�(hjg  h je  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh jb  hhh8hkh:K#ubh�)��}�(hX"  It is difficult to associate motion on a trackpoint with a physical
reference. Unlike mice or touchpads where the motion can be
measured in mm, the trackpoint only responds to pressure. Without special
equipment it is impossible to measure identical pressure values across
multiple laptops.�h]�hX"  It is difficult to associate motion on a trackpoint with a physical
reference. Unlike mice or touchpads where the motion can be
measured in mm, the trackpoint only responds to pressure. Without special
equipment it is impossible to measure identical pressure values across
multiple laptops.�����}�(hju  h js  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K%h jb  hhubh�)��}�(hX�  The values provided by a trackpoint are motion deltas, usually corresponding
to the pressure applied to the trackstick. For example, pressure towards the
screen on a laptop provides negative y deltas. The reporting rate increases
as the pressure increases and once events are reported at the maximum rate,
the delta values increase. The figure below shows a rough illustration of
this concept. As the pressure
decreases, the delta decrease first, then the reporting rate until the
trackpoint is in a neutral state and no events are reported. Trackpoint data
is hard to generalize, see
`Observations on trackpoint input data
<a href="https://who-t.blogspot.com/2018/06/observations-on-trackpoint-input-data.html">`_
for more details.�h]�(hXI  The values provided by a trackpoint are motion deltas, usually corresponding
to the pressure applied to the trackstick. For example, pressure towards the
screen on a laptop provides negative y deltas. The reporting rate increases
as the pressure increases and once events are reported at the maximum rate,
the delta values increase. The figure below shows a rough illustration of
this concept. As the pressure
decreases, the delta decrease first, then the reporting rate until the
trackpoint is in a neutral state and no events are reported. Trackpoint data
is hard to generalize, see
�����}�(hXI  The values provided by a trackpoint are motion deltas, usually corresponding
to the pressure applied to the trackstick. For example, pressure towards the
screen on a laptop provides negative y deltas. The reporting rate increases
as the pressure increases and once events are reported at the maximum rate,
the delta values increase. The figure below shows a rough illustration of
this concept. As the pressure
decreases, the delta decrease first, then the reporting rate until the
trackpoint is in a neutral state and no events are reported. Trackpoint data
is hard to generalize, see
�h j�  hhh8Nh:Nubh)��}�(h��`Observations on trackpoint input data
<a href="https://who-t.blogspot.com/2018/06/observations-on-trackpoint-input-data.html">`_�h]�h�%Observations on trackpoint input data�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]��name��%Observations on trackpoint input data��refuri��Uahref="https://who-t.blogspot.com/2018/06/observations-on-trackpoint-input-data.html"�uh0hh j�  ubh^)��}�(h�Y
<a href="https://who-t.blogspot.com/2018/06/observations-on-trackpoint-input-data.html">�h]�h!}�(h#]��%observations-on-trackpoint-input-data�ah%]�h']��%observations on trackpoint input data�ah)]�h+]��refuri�j�  uh0h]�
referenced�Kh j�  ubh�
for more details.�����}�(h�
for more details.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K+h jb  hhubh�)��}�(hhh]�(h�)��}�(h��.. figure:: trackpoint-delta-illustration.svg
    :align: center

    Illustration of the relationship between reporting rate and delta values on a trackpoint
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��!trackpoint-delta-illustration.svg�h�}�h�j�  suh0h�h j�  h8hkh:K;ubh�)��}�(h�XIllustration of the relationship between reporting rate and delta values on a trackpoint�h]�h�XIllustration of the relationship between reporting rate and delta values on a trackpoint�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K;h j�  ubeh!}�(h#]��id2�ah%]�h']�h)]�h+]�h��center�uh0h�h:K;h jb  hhh8hkubh�)��}�(hX  The delta range itself can vary greatly between laptops, some devices send a
maximum delta value of 30, others can go beyond 100. However, the useful
delta range is a fraction of the maximum range. It is uncomfortable to exert
sufficient pressure to even get close to the maximum ranges.�h]�hX  The delta range itself can vary greatly between laptops, some devices send a
maximum delta value of 30, others can go beyond 100. However, the useful
delta range is a fraction of the maximum range. It is uncomfortable to exert
sufficient pressure to even get close to the maximum ranges.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K=h jb  hhubh�)��}�(h�vlibinput provides a :ref:`Magic Trackpoint Multiplier
<trackpoint_multiplier>` to normalize the trackpoint input data.�h]�(h�libinput provides a �����}�(h�libinput provides a �h j�  hhh8Nh:Nubh�)��}�(h�::ref:`Magic Trackpoint Multiplier
<trackpoint_multiplier>`�h]�h�)��}�(hj�  h]�h�Magic Trackpoint Multiplier�����}�(hhh j�  ubah!}�(h#]�h%]�(h܌std��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j   �refexplicit��h�trackpoint_multiplier�h�h�h��uh0h�h8hkh:KBh j�  ubh�( to normalize the trackpoint input data.�����}�(h�( to normalize the trackpoint input data.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KBh jb  hhubeh!}�(h#]�(�motion-range-on-trackpoints�jT  eh%]�h']�(�motion range on trackpoints��trackpoint_range�eh)]�h+]�uh0hlh hnhhh8hkh:K#j^  }�j!  jJ  sj`  }�jT  jJ  subeh!}�(h#]�(�trackpoints-and-pointing-sticks�hjeh%]�h']�(�trackpoints and pointing sticks��trackpoints�eh)]�h+]�uh0hlh hhhh8hkh:Kj^  }�j,  h_sj`  }�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�jT  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�(hj]�h_aj  ]�h�ajT  ]�jJ  au�nameids�}�(j,  hjj+  j(  j[  j  jZ  jW  j!  jT  j   j  j�  j�  u�	nametypes�}�(j,  �j+  Nj[  �jZ  Nj!  �j   Nj�  �uh#}�(hjhnj(  hnj  j  jW  j  jT  jb  j  jb  j�  j�  h�h�j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�h�)��}�(hhh]�h�1Hyperlink target "trackpoints" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type��INFO��source�hk�line�Kuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�=Hyperlink target "trackpoint-buttonscroll" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�Kuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�6Hyperlink target "trackpoint-range" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K$uh0j�  ube�transformer�N�
decoration�Nhhub.