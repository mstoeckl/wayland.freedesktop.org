���G      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`67b2e32`�h]�h �	reference���)��}�(h�git commit 67b2e32�h]�h �Text����git commit 67b2e32�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/67b2e32�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7fd1528816a8>`

�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7fd1528816a8>�h]�h�<git commit <function get_git_version_full at 0x7fd1528816a8>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7fd1528816a8>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�
.. _seats:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��seats�uh0h]h:Kh hhhh8�1/home/whot/code/libinput/build/doc/user/seats.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Seats�h]�h�Seats�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(hX  Each device in libinput is assigned to one seat.
A seat has two identifiers, the physical name and the logical name. The
physical name is summarized as the list of devices a process on the same
physical seat has access to. The logical seat name is the seat name for a
logical group of devices. A compositor may use that to create additional
seats as independent device sets. Alternatively, a compositor may limit
itself to a single logical seat, leaving a second compositor to manage
devices on the other logical seats.�h]�hX  Each device in libinput is assigned to one seat.
A seat has two identifiers, the physical name and the logical name. The
physical name is summarized as the list of devices a process on the same
physical seat has access to. The logical seat name is the seat name for a
logical group of devices. A compositor may use that to create additional
seats as independent device sets. Alternatively, a compositor may limit
itself to a single logical seat, leaving a second compositor to manage
devices on the other logical seats.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh^)��}�(h�.. _seats_overview:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�seats-overview�uh0h]h:Kh hnhhh8hkubhm)��}�(hhh]�(hr)��}�(h�Overview�h]�h�Overview�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh h�hhh8hkh:Kubh�)��}�(h�JBelow is an illustration of how physical seats and logical seats interact:�h]�h�JBelow is an illustration of how physical seats and logical seats interact:�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�hhub�sphinx.ext.graphviz��graphviz���)��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]��code�X�  digraph seats
{
  rankdir="BT";
  node [
    shape="box";
  ]

  kernel [label="Kernel"];

  event0 [URL="\ref libinput_event"];
  event1 [URL="\ref libinput_event"];
  event2 [URL="\ref libinput_event"];
  event3 [URL="\ref libinput_event"];

  pseat0 [label="phys seat0"; URL="\ref libinput_seat_get_physical_name"];
  pseat1 [label="phys seat1"; URL="\ref libinput_seat_get_physical_name"];

  lseatA [label="logical seat A"; URL="\ref libinput_seat_get_logical_name"];
  lseatB [label="logical seat B"; URL="\ref libinput_seat_get_logical_name"];
  lseatC [label="logical seat C"; URL="\ref libinput_seat_get_logical_name"];

  ctx1 [label="libinput context 1"; URL="\ref libinput"];
  ctx2 [label="libinput context 2"; URL="\ref libinput"];

  dev1 [label="device 'Foo'"];
  dev2 [label="device 'Bar'"];
  dev3 [label="device 'Spam'"];
  dev4 [label="device 'Egg'"];

  kernel -> event0
  kernel -> event1
  kernel -> event2
  kernel -> event3

  event0 -> pseat0
  event1 -> pseat0
  event2 -> pseat0
  event3 -> pseat1

  pseat0 -> ctx1
  pseat1 -> ctx2

  ctx1 -> lseatA
  ctx1 -> lseatB
  ctx2 -> lseatC

  lseatA -> dev1
  lseatA -> dev2
  lseatB -> dev3
  lseatC -> dev4
}
��options�}�uh0h�h h�hhh8hkh:Kubh�)��}�(hX  The devices "Foo", "Bar" and "Spam" share the same physical seat and are
thus available in the same libinput context. Only "Foo" and "Bar" share the
same logical seat. The device "Egg" is not available in the libinput context
associated with the physical seat 0.�h]�hX  The devices “Foo”, “Bar” and “Spam” share the same physical seat and are
thus available in the same libinput context. Only “Foo” and “Bar” share the
same logical seat. The device “Egg” is not available in the libinput context
associated with the physical seat 0.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�hhubh�)��}�(h��The above graph is for illustration purposes only. In libinput, a struct
**libinput_seat** comprises both physical seat and logical seat. From a
caller's point-of-view the above device layout is presented as:�h]�(h�IThe above graph is for illustration purposes only. In libinput, a struct
�����}�(h�IThe above graph is for illustration purposes only. In libinput, a struct
�h h�hhh8Nh:Nubh �strong���)��}�(h�**libinput_seat**�h]�h�libinput_seat�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh�x comprises both physical seat and logical seat. From a
caller’s point-of-view the above device layout is presented as:�����}�(h�v comprises both physical seat and logical seat. From a
caller's point-of-view the above device layout is presented as:�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�hhubh�)��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]�h�X9  digraph seats_libinput
{
  rankdir="BT";
  node [
    shape="box";
  ]

  ctx1 [label="libinput context 1"; URL="\ref libinput"];
  ctx2 [label="libinput context 2"; URL="\ref libinput"];

  seat0 [ label="seat phys 0 logical A"];
  seat1 [ label="seat phys 0 logical B"];
  seat2 [ label="seat phys 1 logical C"];

  dev1 [label="device 'Foo'"];
  dev2 [label="device 'Bar'"];
  dev3 [label="device 'Spam'"];
  dev4 [label="device 'Egg'"];

  ctx1 -> dev1
  ctx1 -> dev2
  ctx1 -> dev3
  ctx2 -> dev4

  dev1 -> seat0
  dev2 -> seat0
  dev3 -> seat1
  dev4 -> seat2
}
�h�}�uh0h�h h�hhh8hkh:K$ubh�)��}�(h��Thus, devices "Foo" and "Bar" both reference the same struct
**libinput_seat**, all other devices reference their own respective seats.�h]�(h�EThus, devices “Foo” and “Bar” both reference the same struct
�����}�(h�=Thus, devices "Foo" and "Bar" both reference the same struct
�h j  hhh8Nh:Nubh�)��}�(h�**libinput_seat**�h]�h�libinput_seat�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  ubh�9, all other devices reference their own respective seats.�����}�(h�9, all other devices reference their own respective seats.�h j  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K%h h�hhubh^)��}�(h�.. _seats_and_features:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�seats-and-features�uh0h]h:K-h h�hhh8hkubeh!}�(h#]�(�overview�h�eh%]�h']�(�overview��seats_overview�eh)]�h+]�uh0hlh hnhhh8hkh:K�expect_referenced_by_name�}�j;  h�s�expect_referenced_by_id�}�h�h�subhm)��}�(hhh]�(hr)��}�(h�The effect of seat assignment�h]�h�The effect of seat assignment�����}�(hjG  h jE  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh jB  hhh8hkh:K,ubh�)��}�(h��A logical set is interpreted as a group of devices that usually belong to a
single user that interacts with a computer. Thus, the devices are
semantically related. This means for devices within the same logical seat:�h]�h��A logical set is interpreted as a group of devices that usually belong to a
single user that interacts with a computer. Thus, the devices are
semantically related. This means for devices within the same logical seat:�����}�(hjU  h jS  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K.h jB  hhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�pif the same button is pressed on different devices, the button should only
be considered logically pressed once.�h]�h�)��}�(h�pif the same button is pressed on different devices, the button should only
be considered logically pressed once.�h]�h�pif the same button is pressed on different devices, the button should only
be considered logically pressed once.�����}�(hjn  h jl  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K2h jh  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jf  h jc  hhh8hkh:Nubjg  )��}�(h�}if the same button is released on one device, the button should be
considered logically down if still down on another device.�h]�h�)��}�(h�}if the same button is released on one device, the button should be
considered logically down if still down on another device.�h]�h�}if the same button is released on one device, the button should be
considered logically down if still down on another device.�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K4h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jf  h jc  hhh8hkh:Nubjg  )��}�(h�wif two different buttons or keys are pressed on different devices, the
logical state is that of both buttons/keys down.�h]�h�)��}�(h�wif two different buttons or keys are pressed on different devices, the
logical state is that of both buttons/keys down.�h]�h�wif two different buttons or keys are pressed on different devices, the
logical state is that of both buttons/keys down.�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K6h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jf  h jc  hhh8hkh:Nubjg  )��}�(h�]if a button is pressed on one device and another device moves, this should
count as dragging.�h]�h�)��}�(h�]if a button is pressed on one device and another device moves, this should
count as dragging.�h]�h�]if a button is pressed on one device and another device moves, this should
count as dragging.�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K8h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jf  h jc  hhh8hkh:Nubjg  )��}�(h�]if two touches are down on different devices, the logical state is that of
two touches down.
�h]�h�)��}�(h�\if two touches are down on different devices, the logical state is that of
two touches down.�h]�h�\if two touches are down on different devices, the logical state is that of
two touches down.�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K:h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jf  h jc  hhh8hkh:Nubeh!}�(h#]�h%]�h']�h)]�h+]��bullet��-�uh0ja  h8hkh:K2h jB  hhubh�)��}�(h��libinput provides functions to aid with the above:
**libinput_event_pointer_get_seat_button_count()**,
**libinput_event_keyboard_get_seat_key_count()**, and
**libinput_event_touch_get_seat_slot()**.�h]�(h�3libinput provides functions to aid with the above:
�����}�(h�3libinput provides functions to aid with the above:
�h j�  hhh8Nh:Nubh�)��}�(h�2**libinput_event_pointer_get_seat_button_count()**�h]�h�.libinput_event_pointer_get_seat_button_count()�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�,
�����}�(h�,
�h j�  hhh8Nh:Nubh�)��}�(h�0**libinput_event_keyboard_get_seat_key_count()**�h]�h�,libinput_event_keyboard_get_seat_key_count()�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�, and
�����}�(h�, and
�h j�  hhh8Nh:Nubh�)��}�(h�(**libinput_event_touch_get_seat_slot()**�h]�h�$libinput_event_touch_get_seat_slot()�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�.�����}�(h�.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K=h jB  hhubh�)��}�(hXI  Internally, libinput counts devices within the same logical seat as related.
Cross-device features only activate if all required devices are in the same
logical seat. For example, libinput will only activate the top software
buttons (see :ref:`t440_support`) if both trackstick and touchpad are assigned
to the same logical seat.�h]�(h��Internally, libinput counts devices within the same logical seat as related.
Cross-device features only activate if all required devices are in the same
logical seat. For example, libinput will only activate the top software
buttons (see �����}�(h��Internally, libinput counts devices within the same logical seat as related.
Cross-device features only activate if all required devices are in the same
logical seat. For example, libinput will only activate the top software
buttons (see �h j0  hhh8Nh:Nub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`t440_support`�h]�h �inline���)��}�(hj>  h]�h�t440_support�����}�(hhh jB  ubah!}�(h#]�h%]�(�xref��std��std-ref�eh']�h)]�h+]�uh0j@  h j<  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�jM  �refexplicit���	reftarget��t440_support��refdoc��seats��refwarn��uh0j:  h8hkh:KBh j0  ubh�H) if both trackstick and touchpad are assigned
to the same logical seat.�����}�(h�H) if both trackstick and touchpad are assigned
to the same logical seat.�h j0  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KBh jB  hhubh^)��}�(h�.. _changing_seats:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�changing-seats�uh0h]h:KNh jB  hhh8hkubeh!}�(h#]�(�the-effect-of-seat-assignment�j4  eh%]�h']�(�the effect of seat assignment��seats_and_features�eh)]�h+]�uh0hlh hnhhh8hkh:K,j>  }�j}  j*  sj@  }�j4  j*  subhm)��}�(hhh]�(hr)��}�(h�Changing seats�h]�h�Changing seats�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:KMubh�)��}�(h��A device may change the logical seat it is assigned to at runtime with
**libinput_device_set_seat_logical_name()**. The physical seat is immutable and
may not be changed.�h]�(h�GA device may change the logical seat it is assigned to at runtime with
�����}�(h�GA device may change the logical seat it is assigned to at runtime with
�h j�  hhh8Nh:Nubh�)��}�(h�+**libinput_device_set_seat_logical_name()**�h]�h�'libinput_device_set_seat_logical_name()�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�8. The physical seat is immutable and
may not be changed.�����}�(h�8. The physical seat is immutable and
may not be changed.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KOh j�  hhubh�)��}�(h��Changing the logical seat for a device is equivalent to unplugging the
device and plugging it back in with the new logical seat. No device state
carries over across a logical seat change.�h]�h��Changing the logical seat for a device is equivalent to unplugging the
device and plugging it back in with the new logical seat. No device state
carries over across a logical seat change.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KSh j�  hhubeh!}�(h#]�(jv  �id2�eh%]�h']�(�changing seats��changing_seats�eh)]�h+]�uh0hlh hnhhh8hkh:KMj>  }�j�  jl  sj@  }�jv  jl  subeh!}�(h#]�(hj�id1�eh%]�h']��seats�ah)]��seats�ah+]�uh0hlh hhhh8hkh:K�
referenced�Kj>  }�j�  h_sj@  }�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�(hj]�h_ah�]�h�aj4  ]�j*  ajv  ]�jl  au�nameids�}�(j�  hjj;  h�j:  j7  j}  j4  j|  jy  j�  jv  j�  j�  u�	nametypes�}�(j�  �j;  �j:  Nj}  �j|  Nj�  �j�  Nuh#}�(hjhnj�  hnh�h�j7  h�j4  jB  jy  jB  jv  j�  j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�h �system_message���)��}�(hhh]�h�)��}�(h�(Duplicate implicit target name: "seats".�h]�h�,Duplicate implicit target name: “seats”.�����}�(hhh j`  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j]  ubah!}�(h#]�h%]�h']�h)]�h+]�j�  a�level�K�type��INFO��source�hk�line�Kuh0j[  h hnhhh8hkh:Kuba�transform_messages�]�(j\  )��}�(hhh]�h�)��}�(hhh]�h�+Hyperlink target "seats" is not referenced.�����}�(hhh j~  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j{  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�jv  �source�hk�line�Kuh0j[  ubj\  )��}�(hhh]�h�)��}�(hhh]�h�4Hyperlink target "seats-overview" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�jv  �source�hk�line�Kuh0j[  ubj\  )��}�(hhh]�h�)��}�(hhh]�h�8Hyperlink target "seats-and-features" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�jv  �source�hk�line�K-uh0j[  ubj\  )��}�(hhh]�h�)��}�(hhh]�h�4Hyperlink target "changing-seats" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�jv  �source�hk�line�KNuh0j[  ube�transformer�N�
decoration�Nhhub.