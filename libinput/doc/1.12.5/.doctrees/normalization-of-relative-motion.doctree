��7@      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`7d25d9e`�h]�h �	reference���)��}�(h�git commit 7d25d9e�h]�h �Text����git commit 7d25d9e�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/7d25d9e�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f4b9dfe32f0>`

�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7f4b9dfe32f0>�h]�h�<git commit <function get_git_version_full at 0x7f4b9dfe32f0>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f4b9dfe32f0>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _motion_normalization:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��motion-normalization�uh0h]h:Kh hhhh8�L/home/whot/code/libinput/build/doc/user/normalization-of-relative-motion.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h� Normalization of relative motion�h]�h� Normalization of relative motion�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(hX  Most relative input devices generate input in so-called "mickeys". A
mickey is in device-specific units that depend on the resolution
of the sensor. Most optical mice use sensors with 1000dpi resolution, but
some devices range from 100dpi to well above 8000dpi.�h]�hX	  Most relative input devices generate input in so-called “mickeys”. A
mickey is in device-specific units that depend on the resolution
of the sensor. Most optical mice use sensors with 1000dpi resolution, but
some devices range from 100dpi to well above 8000dpi.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(hX*  Without a physical reference point, a relative coordinate cannot be
interpreted correctly. A delta of 10 mickeys may be a millimeter of
physical movement or 10 millimeters, depending on the sensor. This
affects pointer acceleration in libinput and interpretation of relative
coordinates in callers.�h]�hX*  Without a physical reference point, a relative coordinate cannot be
interpreted correctly. A delta of 10 mickeys may be a millimeter of
physical movement or 10 millimeters, depending on the sensor. This
affects pointer acceleration in libinput and interpretation of relative
coordinates in callers.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(hX  libinput does partial normalization of relative input. For devices with a
resolution of 1000dpi and higher, motion events are normalized to a default
of 1000dpi before pointer acceleration is applied. As a result, devices with
1000dpi and above feel the same.�h]�hX  libinput does partial normalization of relative input. For devices with a
resolution of 1000dpi and higher, motion events are normalized to a default
of 1000dpi before pointer acceleration is applied. As a result, devices with
1000dpi and above feel the same.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(hX�  Devices below 1000dpi are not normalized (normalization of a 1-device unit
movement on a 400dpi mouse would cause a 2.5 pixel movement). Instead,
libinput applies a dpi-dependent acceleration function. At low speeds, a
1-device unit movement usually translates into a 1-pixel movements. As the
movement speed increases, acceleration is applied - at high speeds a low-dpi
device will roughly feel the same as a higher-dpi mouse.�h]�hX�  Devices below 1000dpi are not normalized (normalization of a 1-device unit
movement on a 400dpi mouse would cause a 2.5 pixel movement). Instead,
libinput applies a dpi-dependent acceleration function. At low speeds, a
1-device unit movement usually translates into a 1-pixel movements. As the
movement speed increases, acceleration is applied - at high speeds a low-dpi
device will roughly feel the same as a higher-dpi mouse.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(h��This normalization only applies to accelerated coordinates, unaccelerated
coordinates are left in device-units. It is up to the caller to interpret
those coordinates correctly.�h]�h��This normalization only applies to accelerated coordinates, unaccelerated
coordinates are left in device-units. It is up to the caller to interpret
those coordinates correctly.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh^)��}�(h�".. _motion_normalization_touchpad:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�motion-normalization-touchpad�uh0h]h:K'h hnhhh8hkubhm)��}�(hhh]�(hr)��}�(h�%Normalization of touchpad coordinates�h]�h�%Normalization of touchpad coordinates�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh h�hhh8hkh:K&ubh�)��}�(h��Touchpads may have a different resolution for the horizontal and vertical
axis. Interpreting coordinates from the touchpad without taking resolution
into account results in uneven motion.�h]�h��Touchpads may have a different resolution for the horizontal and vertical
axis. Interpreting coordinates from the touchpad without taking resolution
into account results in uneven motion.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K(h h�hhubh�)��}�(h��libinput scales unaccelerated touchpad motion to the resolution of the
touchpad's x axis, i.e. the unaccelerated value for the y axis is:
``y = (x / resolution_x) * resolution_y``.�h]�(h��libinput scales unaccelerated touchpad motion to the resolution of the
touchpad’s x axis, i.e. the unaccelerated value for the y axis is:
�����}�(h��libinput scales unaccelerated touchpad motion to the resolution of the
touchpad's x axis, i.e. the unaccelerated value for the y axis is:
�h h�hhh8Nh:Nubh �literal���)��}�(h�)``y = (x / resolution_x) * resolution_y``�h]�h�%y = (x / resolution_x) * resolution_y�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh�.�����}�(h�.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K,h h�hhubh^)��}�(h� .. _motion_normalization_tablet:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�motion-normalization-tablet�uh0h]h:K5h h�hhh8hkubeh!}�(h#]�(�%normalization-of-touchpad-coordinates�h�eh%]�h']�(�%normalization of touchpad coordinates��motion_normalization_touchpad�eh)]�h+]�uh0hlh hnhhh8hkh:K&�expect_referenced_by_name�}�j(  h�s�expect_referenced_by_id�}�h�h�subhm)��}�(hhh]�(hr)��}�(h�#Normalization of tablet coordinates�h]�h�#Normalization of tablet coordinates�����}�(hj4  h j2  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j/  hhh8hkh:K4ubh�)��}�(h�!See :ref:`tablet-relative-motion`�h]�(h�See �����}�(h�See �h j@  hhh8Nh:Nub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`tablet-relative-motion`�h]�h �inline���)��}�(hjN  h]�h�tablet-relative-motion�����}�(hhh jR  ubah!}�(h#]�h%]�(�xref��std��std-ref�eh']�h)]�h+]�uh0jP  h jL  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j]  �refexplicit���	reftarget��tablet-relative-motion��refdoc�� normalization-of-relative-motion��refwarn��uh0jJ  h8hkh:K6h j@  ubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K6h j/  hhubh^)��}�(h�'.. _motion_normalization_customization:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�"motion-normalization-customization�uh0h]h:K=h j/  hhh8hkubeh!}�(h#]�(�#normalization-of-tablet-coordinates�j!  eh%]�h']�(�#normalization of tablet coordinates��motion_normalization_tablet�eh)]�h+]�uh0hlh hnhhh8hkh:K4j+  }�j�  j  sj-  }�j!  j  subhm)��}�(hhh]�(hr)��}�(h�Setting custom DPI settings�h]�h�Setting custom DPI settings�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:K<ubh�)��}�(hX�  Devices usually do not advertise their resolution and libinput relies on
the udev property **MOUSE_DPI** for this information. This property is usually
set via the
`udev hwdb <http://cgit.freedesktop.org/systemd/systemd/tree/hwdb/70-mouse.hwdb>`_.
The ``mouse-dpi-tool`` utility provided by
`libevdev <https://freedesktop.org/wiki/Software/libevdev/>`_ should be
used to measure a device's resolution.�h]�(h�[Devices usually do not advertise their resolution and libinput relies on
the udev property �����}�(h�[Devices usually do not advertise their resolution and libinput relies on
the udev property �h j�  hhh8Nh:Nubh �strong���)��}�(h�**MOUSE_DPI**�h]�h�	MOUSE_DPI�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j�  ubh�< for this information. This property is usually
set via the
�����}�(h�< for this information. This property is usually
set via the
�h j�  hhh8Nh:Nubh)��}�(h�R`udev hwdb <http://cgit.freedesktop.org/systemd/systemd/tree/hwdb/70-mouse.hwdb>`_�h]�h�	udev hwdb�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]��name��	udev hwdb��refuri��Chttp://cgit.freedesktop.org/systemd/systemd/tree/hwdb/70-mouse.hwdb�uh0hh j�  ubh^)��}�(h�F <http://cgit.freedesktop.org/systemd/systemd/tree/hwdb/70-mouse.hwdb>�h]�h!}�(h#]��	udev-hwdb�ah%]�h']��	udev hwdb�ah)]�h+]��refuri�j�  uh0h]�
referenced�Kh j�  ubh�.
The �����}�(h�.
The �h j�  hhh8Nh:Nubh�)��}�(h�``mouse-dpi-tool``�h]�h�mouse-dpi-tool�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh� utility provided by
�����}�(h� utility provided by
�h j�  hhh8Nh:Nubh)��}�(h�=`libevdev <https://freedesktop.org/wiki/Software/libevdev/>`_�h]�h�libevdev�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]��name��libevdev�j�  �/https://freedesktop.org/wiki/Software/libevdev/�uh0hh j�  ubh^)��}�(h�2 <https://freedesktop.org/wiki/Software/libevdev/>�h]�h!}�(h#]��libevdev�ah%]�h']��libevdev�ah)]�h+]��refuri�j  uh0h]j�  Kh j�  ubh�3 should be
used to measure a device’s resolution.�����}�(h�1 should be
used to measure a device's resolution.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K>h j�  hhubh�)��}�(h�<The format of the property for single-resolution mice is: ::�h]�h�9The format of the property for single-resolution mice is:�����}�(h�9The format of the property for single-resolution mice is:�h j  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KFh j�  hhubh �literal_block���)��}�(h�MOUSE_DPI=resolution@frequency�h]�h�MOUSE_DPI=resolution@frequency�����}�(hhh j.  ubah!}�(h#]�h%]�h']�h)]�h+]��	xml:space��preserve�uh0j,  h:KMh j�  hhh8hkubh�)��}�(h��The resolution is in dots per inch, the frequency in Hz.
The format of the property for multi-resolution mice may list multiple
resolutions and frequencies: ::�h]�h��The resolution is in dots per inch, the frequency in Hz.
The format of the property for multi-resolution mice may list multiple
resolutions and frequencies:�����}�(h��The resolution is in dots per inch, the frequency in Hz.
The format of the property for multi-resolution mice may list multiple
resolutions and frequencies:�h j>  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KJh j�  hhubj-  )��}�(h�MOUSE_DPI=r1@f1 *r2@f2 r3@f3�h]�h�MOUSE_DPI=r1@f1 *r2@f2 r3@f3�����}�(hhh jM  ubah!}�(h#]�h%]�h']�h)]�h+]�j<  j=  uh0j,  h:KSh j�  hhh8hkubh�)��}�(h�9The default frequency must be pre-fixed with an asterisk.�h]�h�9The default frequency must be pre-fixed with an asterisk.�����}�(hj]  h j[  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KPh j�  hhubh�)��}�(h�/For example, these two properties are valid: ::�h]�h�,For example, these two properties are valid:�����}�(h�,For example, these two properties are valid:�h ji  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KRh j�  hhubj-  )��}�(h�>MOUSE_DPI=800@125
MOUSE_DPI=400@125 800@125 *1000@500 5500@500�h]�h�>MOUSE_DPI=800@125
MOUSE_DPI=400@125 800@125 *1000@500 5500@500�����}�(hhh jx  ubah!}�(h#]�h%]�h']�h)]�h+]�j<  j=  uh0j,  h:KYh j�  hhh8hkubh�)��}�(h�yThe behavior for a malformed property is undefined. If the property is
unset, libinput assumes the resolution is 1000dpi.�h]�h�yThe behavior for a malformed property is undefined. If the property is
unset, libinput assumes the resolution is 1000dpi.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KWh j�  hhubh�)��}�(h��Note that HW does not usually provide information about run-time
resolution changes, libinput will thus not detect when a resolution
changes to the non-default value.�h]�h��Note that HW does not usually provide information about run-time
resolution changes, libinput will thus not detect when a resolution
changes to the non-default value.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KZh j�  hhubeh!}�(h#]�(�setting-custom-dpi-settings�j�  eh%]�h']�(�setting custom dpi settings��"motion_normalization_customization�eh)]�h+]�uh0hlh hnhhh8hkh:K<j+  }�j�  jw  sj-  }�j�  jw  subeh!}�(h#]�(� normalization-of-relative-motion�hjeh%]�h']�(� normalization of relative motion��motion_normalization�eh)]�h+]�uh0hlh hhhh8hkh:Kj+  }�j�  h_sj-  }�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�(hj]�h_ah�]�h�aj!  ]�j  aj�  ]�jw  au�nameids�}�(j�  hjj�  j�  j(  h�j'  j$  j�  j!  j�  j�  j�  j�  j�  j�  j�  j�  j  j  u�	nametypes�}�(j�  �j�  Nj(  �j'  Nj�  �j�  Nj�  �j�  Nj�  �j  �uh#}�(hjhnj�  hnh�h�j$  h�j!  j/  j�  j/  j�  j�  j�  j�  j�  j�  j  j  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�h�)��}�(hhh]�h�:Hyperlink target "motion-normalization" is not referenced.�����}�(hhh j@  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j=  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type��INFO��source�hk�line�Kuh0j;  ubj<  )��}�(hhh]�h�)��}�(hhh]�h�CHyperlink target "motion-normalization-touchpad" is not referenced.�����}�(hhh j[  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jX  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�jU  �source�hk�line�K'uh0j;  ubj<  )��}�(hhh]�h�)��}�(hhh]�h�AHyperlink target "motion-normalization-tablet" is not referenced.�����}�(hhh ju  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jr  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�jU  �source�hk�line�K5uh0j;  ubj<  )��}�(hhh]�h�)��}�(hhh]�h�HHyperlink target "motion-normalization-customization" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�jU  �source�hk�line�K=uh0j;  ube�transformer�N�
decoration�Nhhub.