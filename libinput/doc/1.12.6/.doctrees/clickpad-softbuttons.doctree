��*f      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`9da9118`�h]�h �	reference���)��}�(h�git commit 9da9118�h]�h �Text����git commit 9da9118�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/9da9118�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f81e92b1598>`

�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7f81e92b1598>�h]�h�<git commit <function get_git_version_full at 0x7f81e92b1598>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f81e92b1598>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _clickpad_softbuttons:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��clickpad-softbuttons�uh0h]h:Kh hhhh8�@/home/whot/code/libinput/build/doc/user/clickpad-softbuttons.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�!Clickpad software button behavior�h]�h�!Clickpad software button behavior�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(hX�  "Clickpads" are touchpads without separate physical buttons. Instead, the
whole touchpad acts as a button and left or right button clicks are
distinguished by :ref:`the location of the fingers <software_buttons>` or
the :ref:`number of fingers on the touchpad <clickfinger>`.
"ClickPad" is a trademark by `Synaptics Inc. <http://www.synaptics.com/en/clickpad.php>`_
but for simplicity we refer to any touchpad with the above feature as Clickpad,
regardless of the manufacturer.�h]�(h��“Clickpads” are touchpads without separate physical buttons. Instead, the
whole touchpad acts as a button and left or right button clicks are
distinguished by �����}�(h��"Clickpads" are touchpads without separate physical buttons. Instead, the
whole touchpad acts as a button and left or right button clicks are
distinguished by �h h�hhh8Nh:Nub�sphinx.addnodes��pending_xref���)��}�(h�5:ref:`the location of the fingers <software_buttons>`�h]�h �inline���)��}�(hh�h]�h�the location of the fingers�����}�(hhh h�ubah!}�(h#]�h%]�(�xref��std��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�h��refexplicit���	reftarget��software_buttons��refdoc��clickpad-softbuttons��refwarn��uh0h�h8hkh:Kh h�ubh� or
the �����}�(h� or
the �h h�hhh8Nh:Nubh�)��}�(h�6:ref:`number of fingers on the touchpad <clickfinger>`�h]�h�)��}�(hh�h]�h�!number of fingers on the touchpad�����}�(hhh h�ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�hǌrefexplicit��h��clickfinger�h�h�h��uh0h�h8hkh:Kh h�ubh�#.
“ClickPad” is a trademark by �����}�(h�.
"ClickPad" is a trademark by �h h�hhh8Nh:Nubh)��}�(h�<`Synaptics Inc. <http://www.synaptics.com/en/clickpad.php>`_�h]�h�Synaptics Inc.�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]��name��Synaptics Inc.��refuri��(http://www.synaptics.com/en/clickpad.php�uh0hh h�ubh^)��}�(h�+ <http://www.synaptics.com/en/clickpad.php>�h]�h!}�(h#]��synaptics-inc�ah%]�h']��synaptics inc.�ah)]�h+]��refuri�h�uh0h]�
referenced�Kh h�ubh�p
but for simplicity we refer to any touchpad with the above feature as Clickpad,
regardless of the manufacturer.�����}�(h�p
but for simplicity we refer to any touchpad with the above feature as Clickpad,
regardless of the manufacturer.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(hXH  The kernel marks clickpads with the
`INPUT_PROP_BUTTONPAD <https://www.kernel.org/doc/Documentation/input/event-codes.txt>`_
property. Without this property, libinput would not know whether a touchpad
is a clickpad or not. To perform a right-click on a Clickpad, libinput
provides :ref:`software_buttons` and :ref:`clickfinger`.�h]�(h�$The kernel marks clickpads with the
�����}�(h�$The kernel marks clickpads with the
�h j  hhh8Nh:Nubh)��}�(h�X`INPUT_PROP_BUTTONPAD <https://www.kernel.org/doc/Documentation/input/event-codes.txt>`_�h]�h�INPUT_PROP_BUTTONPAD�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]��name��INPUT_PROP_BUTTONPAD�h�>https://www.kernel.org/doc/Documentation/input/event-codes.txt�uh0hh j  ubh^)��}�(h�A <https://www.kernel.org/doc/Documentation/input/event-codes.txt>�h]�h!}�(h#]��input-prop-buttonpad�ah%]�h']��input_prop_buttonpad�ah)]�h+]��refuri�j   uh0h]h�Kh j  ubh��
property. Without this property, libinput would not know whether a touchpad
is a clickpad or not. To perform a right-click on a Clickpad, libinput
provides �����}�(h��
property. Without this property, libinput would not know whether a touchpad
is a clickpad or not. To perform a right-click on a Clickpad, libinput
provides �h j  hhh8Nh:Nubh�)��}�(h�:ref:`software_buttons`�h]�h�)��}�(hj5  h]�h�software_buttons�����}�(hhh j7  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j3  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�jA  �refexplicit��h��software_buttons�h�h�h��uh0h�h8hkh:Kh j  ubh� and �����}�(h� and �h j  hhh8Nh:Nubh�)��}�(h�:ref:`clickfinger`�h]�h�)��}�(hjX  h]�h�clickfinger�����}�(hhh jZ  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h jV  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�jd  �refexplicit��h��clickfinger�h�h�h��uh0h�h8hkh:Kh j  ubh�.�����}�(h�.�h j  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh �note���)��}�(h��The term "click" refers refer to a physical button press
and/or release of the touchpad, the term "button event" refers to
the events generated by libinput in response to a click.�h]�h�)��}�(h��The term "click" refers refer to a physical button press
and/or release of the touchpad, the term "button event" refers to
the events generated by libinput in response to a click.�h]�h��The term “click” refers refer to a physical button press
and/or release of the touchpad, the term “button event” refers to
the events generated by libinput in response to a click.�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h hnhhh8hkh:Nubh^)��}�(h�.. _software_buttons:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�software-buttons�uh0h]h:Kh hnhhh8hkubhm)��}�(hhh]�(hr)��}�(h�Software button areas�h]�h�Software button areas�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:Kubh�)��}�(h��The bottom of the touchpad is split into three distinct areas generate left,
middle or right button events on click. The height of the button area
depends on the hardware but is usually around 10mm.�h]�h��The bottom of the touchpad is split into three distinct areas generate left,
middle or right button events on click. The height of the button area
depends on the hardware but is usually around 10mm.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j�  hhubh �figure���)��}�(hhh]�(h �image���)��}�(h�s.. figure :: software-buttons-visualized.svg
     :align: center

     The locations of the virtual button areas.

�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��software-buttons-visualized.svg��
candidates�}��*�j�  suh0j�  h j�  h8hkh:K&ubh �caption���)��}�(h�*The locations of the virtual button areas.�h]�h�*The locations of the virtual button areas.�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:K&h j�  ubeh!}�(h#]��id2�ah%]�h']�h)]�h+]��align��center�uh0j�  h:K&h j�  hhh8hkubh�)��}�(h�ALeft, right and middle button events can be triggered as follows:�h]�h�ALeft, right and middle button events can be triggered as follows:�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K)h j�  hhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�^if a finger is in the main area or the left button area, a click generates
left button events.�h]�h�)��}�(h�^if a finger is in the main area or the left button area, a click generates
left button events.�h]�h�^if a finger is in the main area or the left button area, a click generates
left button events.�����}�(hj  h j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K+h j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j  hhh8hkh:Nubj  )��}�(h�Hif a finger is in the right area, a click generates right button events.�h]�h�)��}�(hj!  h]�h�Hif a finger is in the right area, a click generates right button events.�����}�(hj!  h j#  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K-h j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j  hhh8hkh:Nubj  )��}�(h�Kif a finger is in the middle area, a click generates middle button events.
�h]�h�)��}�(h�Jif a finger is in the middle area, a click generates middle button events.�h]�h�Jif a finger is in the middle area, a click generates middle button events.�����}�(hj<  h j:  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K.h j6  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j  hhh8hkh:Nubeh!}�(h#]�h%]�h']�h)]�h+]��bullet��-�uh0j   h8hkh:K+h j�  hhubj�  )��}�(hhh]�(j�  )��}�(h�x.. figure:: software-buttons.svg
    :align: center

    Left, right and middle-button click with software button areas
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��software-buttons.svg�j�  }�j�  jd  suh0j�  h jV  h8hkh:K3ubj�  )��}�(h�>Left, right and middle-button click with software button areas�h]�h�>Left, right and middle-button click with software button areas�����}�(hjh  h jf  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:K3h jV  ubeh!}�(h#]��id3�ah%]�h']�h)]�h+]�j�  �center�uh0j�  h:K3h j�  hhh8hkubh�)��}�(hX  The middle button is always centered on the touchpad and smaller in size
than the left or right button. The actual size is device-dependent. Many
touchpads do not have visible markings so the exact location of the button
is unfortunately not visibly obvious.�h]�hX  The middle button is always centered on the touchpad and smaller in size
than the left or right button. The actual size is device-dependent. Many
touchpads do not have visible markings so the exact location of the button
is unfortunately not visibly obvious.�����}�(hj~  h j|  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K5h j�  hhubj�  )��}�(h��If :ref:`middle button emulation <middle_button_emulation>` is
enabled on a clickpad, only left and right button areas are
available.�h]�h�)��}�(h��If :ref:`middle button emulation <middle_button_emulation>` is
enabled on a clickpad, only left and right button areas are
available.�h]�(h�If �����}�(h�If �h j�  ubh�)��}�(h�8:ref:`middle button emulation <middle_button_emulation>`�h]�h�)��}�(hj�  h]�h�middle button emulation�����}�(hhh j�  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit��h��middle_button_emulation�h�h�h��uh0h�h8hkh:K:h j�  ubh�J is
enabled on a clickpad, only left and right button areas are
available.�����}�(h�J is
enabled on a clickpad, only left and right button areas are
available.�h j�  ubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K:h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j�  hhh8hkh:Nubh�)��}�(h��If fingers are down in the main area in addition to fingers in the
left or right button area, those fingers are are ignored.
A release event always releases the buttons logically down, regardless of
the current finger position�h]�h��If fingers are down in the main area in addition to fingers in the
left or right button area, those fingers are are ignored.
A release event always releases the buttons logically down, regardless of
the current finger position�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K>h j�  hhubj�  )��}�(hhh]�(j�  )��}�(h��.. figure:: software-buttons-thumbpress.svg
    :align: center

    Only the location of the thumb determines whether it is a left, right or
    middle click.
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��software-buttons-thumbpress.svg�j�  }�j�  j�  suh0j�  h j�  h8hkh:KFubj�  )��}�(h�VOnly the location of the thumb determines whether it is a left, right or
middle click.�h]�h�VOnly the location of the thumb determines whether it is a left, right or
middle click.�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:KFh j�  ubeh!}�(h#]��id4�ah%]�h']�h)]�h+]�j�  �center�uh0j�  h:KFh j�  hhh8hkubh�)��}�(h�<The movement of a finger can alter the button area behavior:�h]�h�<The movement of a finger can alter the button area behavior:�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KIh j�  hhubj  )��}�(hhh]�(j  )��}�(h�}if a finger starts in the main area and moves into the software button
area, the software buttons do not apply to that finger�h]�h�)��}�(h�}if a finger starts in the main area and moves into the software button
area, the software buttons do not apply to that finger�h]�h�}if a finger starts in the main area and moves into the software button
area, the software buttons do not apply to that finger�����}�(hj  h j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KKh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j  hhh8hkh:Nubj  )��}�(h�qonce a finger has moved out of the button area, it cannot move back in and
trigger a right or middle button event�h]�h�)��}�(h�qonce a finger has moved out of the button area, it cannot move back in and
trigger a right or middle button event�h]�h�qonce a finger has moved out of the button area, it cannot move back in and
trigger a right or middle button event�����}�(hj)  h j'  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KMh j#  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j  hhh8hkh:Nubj  )��}�(h�Ia finger moving within the software button area does not move the pointer�h]�h�)��}�(hj=  h]�h�Ia finger moving within the software button area does not move the pointer�����}�(hj=  h j?  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KOh j;  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j  hhh8hkh:Nubj  )��}�(h��once a finger moves out out of the button area it will control the
pointer (this only applies if there is no other finger down on the
touchpad)
�h]�h�)��}�(h��once a finger moves out out of the button area it will control the
pointer (this only applies if there is no other finger down on the
touchpad)�h]�h��once a finger moves out out of the button area it will control the
pointer (this only applies if there is no other finger down on the
touchpad)�����}�(hjX  h jV  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KPh jR  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j  hhh8hkh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�jT  jU  uh0j   h8hkh:KKh j�  hhubj�  )��}�(hhh]�(j�  )��}�(h��.. figure:: software-buttons-conditions.svg
    :align: center

    **Left:** moving a finger into the right button area does not trigger a
    right-button click.
    **Right:** moving within the button areas does not generate pointer
    motion.
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��software-buttons-conditions.svg�j�  }�j�  j~  suh0j�  h jp  h8hkh:KWubj�  )��}�(h��**Left:** moving a finger into the right button area does not trigger a
right-button click.
**Right:** moving within the button areas does not generate pointer
motion.�h]�(h �strong���)��}�(h�	**Left:**�h]�h�Left:�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j�  ubh�S moving a finger into the right button area does not trigger a
right-button click.
�����}�(h�S moving a finger into the right button area does not trigger a
right-button click.
�h j�  ubj�  )��}�(h�
**Right:**�h]�h�Right:�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j�  ubh�A moving within the button areas does not generate pointer
motion.�����}�(h�A moving within the button areas does not generate pointer
motion.�h j�  ubeh!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:KWh jp  ubeh!}�(h#]��id5�ah%]�h']�h)]�h+]�j�  �center�uh0j�  h:KWh j�  hhh8hkubh�)��}�(hX�  On some touchpads, notably the 2015 Lenovo X1 Carbon 3rd series, the very
bottom end of the touchpad is outside of the sensor range but it is possible
to trigger a physical click there. To libinput, the click merely shows up as
a left button click without any positional finger data and it is
impossible to determine whether it is a left or a right click. libinput
ignores such button clicks, this behavior is intentional.�h]�hX�  On some touchpads, notably the 2015 Lenovo X1 Carbon 3rd series, the very
bottom end of the touchpad is outside of the sensor range but it is possible
to trigger a physical click there. To libinput, the click merely shows up as
a left button click without any positional finger data and it is
impossible to determine whether it is a left or a right click. libinput
ignores such button clicks, this behavior is intentional.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K\h j�  hhubh^)��}�(h�.. _clickfinger:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�clickfinger�uh0h]h:Khh j�  hhh8hkubeh!}�(h#]�(�software-button-areas�j�  eh%]�h']�(�software button areas��software_buttons�eh)]�h+]�uh0hlh hnhhh8hkh:K�expect_referenced_by_name�}�j�  j�  s�expect_referenced_by_id�}�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�Clickfinger behavior�h]�h�Clickfinger behavior�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:Kgubh�)��}�(hX$  This is the default behavior on Apple touchpads.
Here, a left, right, middle button event is generated when one, two, or
three fingers are held down on the touchpad when a physical click is
generated. The location of the fingers does not matter and there are no
software-defined button areas.�h]�hX$  This is the default behavior on Apple touchpads.
Here, a left, right, middle button event is generated when one, two, or
three fingers are held down on the touchpad when a physical click is
generated. The location of the fingers does not matter and there are no
software-defined button areas.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kih j�  hhubj�  )��}�(hhh]�(j�  )��}�(h�n.. figure:: clickfinger.svg
    :align: center

    One, two and three-finger click with Clickfinger behavior
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��clickfinger.svg�j�  }�j�  j  suh0j�  h j�  h8hkh:Krubj�  )��}�(h�9One, two and three-finger click with Clickfinger behavior�h]�h�9One, two and three-finger click with Clickfinger behavior�����}�(hj  h j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:Krh j�  ubeh!}�(h#]��id6�ah%]�h']�h)]�h+]�j�  �center�uh0j�  h:Krh j�  hhh8hkubh�)��}�(h��On some touchpads, libinput imposes a limit on how the fingers may be placed
on the touchpad. In the most common use-case this allows for a user to
trigger a click with the thumb while leaving the pointer-moving finger on
the touchpad.�h]�h��On some touchpads, libinput imposes a limit on how the fingers may be placed
on the touchpad. In the most common use-case this allows for a user to
trigger a click with the thumb while leaving the pointer-moving finger on
the touchpad.�����}�(hj'  h j%  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kth j�  hhubj�  )��}�(hhh]�(j�  )��}�(h�n.. figure:: clickfinger-distance.svg
    :align: center

    Illustration of the distance detection algorithm
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��clickfinger-distance.svg�j�  }�j�  jA  suh0j�  h j3  h8hkh:K|ubj�  )��}�(h�0Illustration of the distance detection algorithm�h]�h�0Illustration of the distance detection algorithm�����}�(hjE  h jC  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:K|h j3  ubeh!}�(h#]��id7�ah%]�h']�h)]�h+]�j�  �center�uh0j�  h:K|h j�  hhh8hkubh�)��}�(h��In the illustration above the red area marks the proximity area around the
first finger. Since the thumb is outside of that area libinput considers the
click a single-finger click rather than a two-finger click.�h]�h��In the illustration above the red area marks the proximity area around the
first finger. Since the thumb is outside of that area libinput considers the
click a single-finger click rather than a two-finger click.�����}�(hj[  h jY  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K~h j�  hhubh^)��}�(h�.. _special_clickpads:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�special-clickpads�uh0h]h:K�h j�  hhh8hkubeh!}�(h#]�(�clickfinger-behavior�j�  eh%]�h']�(�clickfinger behavior��clickfinger�eh)]�h+]�uh0hlh hnhhh8hkh:Kgj�  }�jx  j�  sj�  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�Special Clickpads�h]�h�Special Clickpads�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j}  hhh8hkh:K�ubh�)��}�(h��The Lenovo \*40 series laptops have a clickpad that provides two software button sections, one at
the top and one at the bottom. See :ref:`Lenovo \*40 series touchpad support <t440_support>`
for details on the top software button.�h]�(h��The Lenovo *40 series laptops have a clickpad that provides two software button sections, one at
the top and one at the bottom. See �����}�(h��The Lenovo \*40 series laptops have a clickpad that provides two software button sections, one at
the top and one at the bottom. See �h j�  hhh8Nh:Nubh�)��}�(h�9:ref:`Lenovo \*40 series touchpad support <t440_support>`�h]�h�)��}�(hj�  h]�h�"Lenovo *40 series touchpad support�����}�(hhh j�  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit��h��t440_support�h�h�h��uh0h�h8hkh:K�h j�  ubh�(
for details on the top software button.�����}�(h�(
for details on the top software button.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j}  hhubh�)��}�(hX   Some Clickpads, notably some Cypress ones, perform right button detection in
firmware and appear to userspace as if the touchpad had physical buttons.
While physically clickpads, these are not handled by the software and
treated like traditional touchpads.�h]�hX   Some Clickpads, notably some Cypress ones, perform right button detection in
firmware and appear to userspace as if the touchpad had physical buttons.
While physically clickpads, these are not handled by the software and
treated like traditional touchpads.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j}  hhubeh!}�(h#]�(jq  �id1�eh%]�h']�(�special clickpads��special_clickpads�eh)]�h+]�uh0hlh hnhhh8hkh:K�j�  }�j�  jg  sj�  }�jq  jg  subeh!}�(h#]�(�!clickpad-software-button-behavior�hjeh%]�h']�(�!clickpad software button behavior��clickpad_softbuttons�eh)]�h+]�uh0hlh hhhh8hkh:Kj�  }�j�  h_sj�  }�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�(hj]�h_aj�  ]�j�  aj�  ]�j�  ajq  ]�jg  au�nameids�}�(j�  hjj�  j�  h�h�j*  j'  j�  j�  j�  j�  jx  j�  jw  jt  j�  jq  j�  j�  u�	nametypes�}�(j�  �j�  Nh��j*  �j�  �j�  Njx  �jw  Nj�  �j�  Nuh#}�(hjhnj�  hnh�h�j'  j!  j�  j�  j�  j�  j�  j�  jt  j�  jq  j}  j�  j}  j�  j�  jv  jV  j�  j�  j�  jp  j  j�  jS  j3  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�h�)��}�(hhh]�h�:Hyperlink target "clickpad-softbuttons" is not referenced.�����}�(hhh jl  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h ji  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type��INFO��source�hk�line�Kuh0jg  ubjh  )��}�(hhh]�h�)��}�(hhh]�h�6Hyperlink target "software-buttons" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�Kuh0jg  ubjh  )��}�(hhh]�h�)��}�(hhh]�h�1Hyperlink target "clickfinger" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�Khuh0jg  ubjh  )��}�(hhh]�h�)��}�(hhh]�h�7Hyperlink target "special-clickpads" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K�uh0jg  ube�transformer�N�
decoration�Nhhub.